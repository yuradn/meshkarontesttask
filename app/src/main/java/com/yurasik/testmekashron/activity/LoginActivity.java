package com.yurasik.testmekashron.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Loader;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.yurasik.testmekashron.R;
import com.yurasik.testmekashron.utils.NetUtil;
import com.yurasik.testmekashron.utils.UI;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.Manifest.permission.READ_CONTACTS;

public class LoginActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    /**
     * Id to identity READ_CONTACTS permission request.
     */
    private static final int REQUEST_READ_CONTACTS = 1024;

    private static final String TAG = "LoginActivity";
    private UserLoginTask mAuthTask = null;

    // UI references.
    @BindView(R.id.edtLogin)
    protected AutoCompleteTextView edtLogin;
    @BindView(R.id.edtPassword)
    protected EditText edtPassword;
    @BindView(R.id.progressBar)
    protected View progressBar;
    @BindView(R.id.llLoginContainer)
    protected View llLoginContainer;
    @BindView(R.id.tvInfo)
    protected TextView tvInfo;
    @BindView(R.id.btnSignIn)
    protected Button btnSingIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        populateAutoComplete();

        edtPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.edtLogin || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        btnSingIn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                tvInfo.setText("");
                attemptLogin();
            }
        });

    }

    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        edtLogin.setError(null);
        edtPassword.setError(null);

        // Store values at the time of the login attempt.
        String email = edtLogin.getText().toString();
        String password = edtPassword.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password)) {
            edtPassword.setError(getString(R.string.error_field_required));
            focusView = edtPassword;
            cancel = true;
        } else if (!isPasswordValid(password)) {
            edtPassword.setError(getString(R.string.error_invalid_password));
            focusView = edtPassword;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            edtLogin.setError(getString(R.string.error_field_required));
            focusView = edtLogin;
            cancel = true;
        } else if (!isLoginValid(email)) {
            edtLogin.setError(getString(R.string.error_invalid_email));
            focusView = edtLogin;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            mAuthTask = new UserLoginTask(email, password);
            mAuthTask.execute((Void) null);
        }
    }

    private boolean isLoginValid(String login) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(login).matches();
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 0;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_longAnimTime);

            llLoginContainer.setVisibility(show ? View.INVISIBLE : View.VISIBLE);
            tvInfo.setVisibility(show ? View.INVISIBLE : View.VISIBLE);
            llLoginContainer.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    llLoginContainer.setVisibility(show ? View.INVISIBLE : View.VISIBLE);
                    tvInfo.setVisibility(show ? View.INVISIBLE : View.VISIBLE);
                }
            });

            progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
            progressBar.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
            llLoginContainer.setVisibility(show ? View.INVISIBLE : View.VISIBLE);
            tvInfo.setVisibility(show ? View.INVISIBLE : View.VISIBLE);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(LoginActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);
        edtLogin.setAdapter(adapter);
    }


    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String login;
        private final String password;
        private String resultString = "";

        UserLoginTask(String login, String password) {
            this.login = login;
            this.password = password;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            if (!NetUtil.isNetworkAvailable(LoginActivity.this)) {
                resultString = "{Error: No internet connection available!}";
                return false;
            }

            byte[] data = null;
            InputStream is = null;
            WifiManager wifiMgr = (WifiManager) getSystemService(Context.WIFI_SERVICE);
            String postData = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                    "<env:Envelope xmlns:env=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:ns1="
                    + "\"urn:General.Intf-IGeneral\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\""
                    + " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:enc=\"http://"
                    + "www.w3.org/2003/05/soap-encoding\">\n"
                    + "<env:Body>\n"
                    + "<ns1:Login env:encodingStyle=\"http://www.w3.org/2003/05/soap-encoding\">\n"
                    + "<UserName xsi:type=\"xsd:string\">" + login + "</UserName>\n"
                    + "<Password xsi:type=\"xsd:string\">" + password + "</Password>\n"
                    + "<IP xsi:type=\"xsd:string\">" + NetUtil.getLocalIpAddress(wifiMgr) + "</IP>\n"
                    + "</ns1:Login>\n"
                    + "</env:Body>\n"
                    + "</env:Envelope>\n";

            Log.d(TAG, "Post: " + postData);

            String server = "http://isapi.mekashron.com/StartAJob/General.dll/soap/IGeneral";

            try {
                URL url = new URL(server);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestProperty("Accept-Encoding", "identity");
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Accept-Charset", "UTF-8");
                conn.setRequestProperty("User-Agent", "android");
                conn.setUseCaches(false);
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "text/xml; charset=UTF-8");
                conn.setRequestProperty("Content-Length", String.valueOf(postData.length()));
                conn.setRequestProperty("SOAPAction", "urn:General.Intf-IGeneral#Login");

                OutputStream os = new BufferedOutputStream(conn.getOutputStream());
                //data = postData.getBytes("UTF-8");
                data = postData.getBytes();
                os.write(data, 0, data.length);

                os.flush();
                os.close();
                data = null;

                Log.d(TAG, "Connection: " + conn);
                conn.connect();
                int responseCode = conn.getResponseCode();
                Log.d(TAG, "Response code: " + conn.getResponseCode());

                ByteArrayOutputStream baos = new ByteArrayOutputStream();

                if (responseCode == 200) {
                    is = conn.getInputStream();

                    byte[] buffer = new byte[8192]; // Такого вот размера буфер
                    // Далее, например, вот так читаем ответ
                    int bytesRead;
                    while ((bytesRead = is.read(buffer)) != -1) {
                        baos.write(buffer, 0, bytesRead);
                    }
                    data = baos.toByteArray();
                    resultString = new String(data, "UTF-8");
                } else {
                    resultString = "{\"Error\":\"Server error, code: " + responseCode + "\"}";
                    return false;
                }

            } catch (IOException e) {
                resultString = "{\"Error\":" + e.getMessage() + "\"}";
                e.printStackTrace();
                return false;
            }

            if (resultString.contains("ResultCode")) {
                return false;
            } else return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);
            Log.d(TAG, "Result: " + resultString);

            int startResult = resultString.indexOf("{");
            int endResult = resultString.indexOf("}") + 1;

            // Your string JSON here
            String inputJSONString = resultString.substring(startResult, endResult);
            JSONObject jObject = null;
            try {
                jObject = new JSONObject(inputJSONString);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Iterator<String> keys = jObject.keys();

            String value = "";
            while (keys.hasNext()) {
                String key = keys.next();
                try {
                    value += "\n" + key + ": " + jObject.getString(key);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            if (success) {
                tvInfo.setText(R.string.sign_it);
                tvInfo.setTextColor(ContextCompat.getColor(getBaseContext(), R.color.black));
                edtLogin.setEnabled(false);
                edtPassword.setEnabled(false);
                btnSingIn.setEnabled(false);
            } else {
                tvInfo.setText(R.string.error_incorrect_password);
                tvInfo.setTextColor(ContextCompat.getColor(getBaseContext(), R.color.red));
            }
            tvInfo.append(value + "\n");

            UI.hideKeyboard(LoginActivity.this);
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }

    private boolean mayRequestContacts() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(READ_CONTACTS)) {
            Snackbar.make(edtLogin, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
                        }
                    });
        } else {
            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
        }
        return false;
    }

    private void populateAutoComplete() {
        if (!mayRequestContacts()) {
            return;
        }

        getLoaderManager().initLoader(0, null, this);
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_READ_CONTACTS) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                populateAutoComplete();
            }
        }
    }
}

